import { ObjectId } from 'mongodb';
import type { Database } from '../lib/types.ts';
import type { Resolvers, Listing } from '../__generated__/resolvers-types.ts';

interface MongoId extends Listing {
  _id: string;
}
export const resolvers: Resolvers = {
  Query: {
    listings: async (_root: any, _args: {}, { db }: { db: Database }) => {
      return (await db.listings.find({}).toArray()) as unknown as Promise<
        Listing[]
      >; // casting to unknown then to Listing[] due to mongodb type conflicts
    },
  },
  Mutation: {
    deleteListing: async (
      _root: undefined,
      { id }: { id: string },
      { db }: { db: Database }
    ) => {
      const deleteRes = (await db.listings.findOneAndDelete({
        _id: new ObjectId(id),
      })) as unknown as Promise<MongoId>;

      console.log(deleteRes);

      if (!deleteRes) {
        throw new Error('failed to delete listing');
      }

      return deleteRes;
    },
  },
  Listing: {
    id: (listing: MongoId): string => listing._id.toString(),
  },
};
