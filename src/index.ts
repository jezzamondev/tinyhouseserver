import 'dotenv/config';

import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import express from 'express';
import http from 'http';
import cors from 'cors';
import { resolvers } from './graphql/index.ts';
import { connectDatabase } from './database/index.ts';
// import { myTest } from './myTest';
import { readFileSync } from 'fs';
import type { Database } from './lib/types.ts';
import cookieParser from 'cookie-parser';
// import { typeDefs } from './graphql/index.ts';

export interface MyContext {
  token?: string;
  db?: Database;
}

const app = express();

// Our httpServer handles incoming requests to our Express app.
// Below, we tell Apollo Server to "drain" this httpServer,
// enabling our servers to shut down gracefully.
const httpServer = http.createServer(app);

const db = await connectDatabase();
// for testing
// const listings = await db.listings.find({}).toArray();
// console.log('listings!', listings);

// Note: this uses a path relative to the project's
// root directory, which is the current working directory
// if the server is executed using `npm run`.
const typeDefs = readFileSync('./schema.graphql', { encoding: 'utf-8' });

// The ApolloServer constructor requires two parameters: your schema
// definition and your set of resolvers.
// Same ApolloServer initialization as before, plus the drain plugin
// for our httpServer.
const server = new ApolloServer<MyContext>({
  typeDefs,
  resolvers,
  plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
});
// Ensure we wait for our server to start
await server.start();

// Set up our Express middleware to handle CORS, body parsing,
// and our expressMiddleware function.
app.use(
  '/api',
  cors<cors.CorsRequest>(),
  cookieParser(process.env.SECRET),
  express.json(),
  // expressMiddleware accepts the same arguments:
  // an Apollo Server instance and optional configuration options
  expressMiddleware(server, {
    context: async ({ req, res }) => ({ token: req.headers.token, db, req, res }),
  })
);

// Modified server startup
await new Promise<void>((resolve) =>
  httpServer.listen({ port: 9000 }, resolve)
);
console.log(`🚀 Server ready at http://localhost:9000/`);
