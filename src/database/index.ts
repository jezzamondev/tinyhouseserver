import { MongoClient, ServerApiVersion } from 'mongodb';
import type { Database, Booking, User, Listing } from '../lib/types';

const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_USER_PASSWORD}@${process.env.DB_CLUSTER}.mongodb.net`;

export const connectDatabase = async (): Promise<Database> => {
  const client = await MongoClient.connect(uri, {
    serverApi: {
      version: ServerApiVersion.v1,
      strict: true,
      deprecationErrors: true,
    },
    // useNewUrlParser: true,
    // useUnifiedTopology: true, //avoid deprecation warnings
  });
  const db = client.db('tinyhouse');
  console.log('mongodb ready');

  return {
    bookings: db.collection<Booking>('bookings'),
    listings: db.collection<Listing>('listings'),
    users: db.collection<User>('users'),
  };
};
