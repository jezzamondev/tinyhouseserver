// import { gql } from 'apollo-server-express';

export const typeDefs = `#graphql
  type Viewer {
    id: ID
    token: String
    avatar: String
    hasWallet: Boolean
    didRequest: Boolean!
  }

  type Listing {
    id: ID!
    title: String!
    image: String!
    address: String!
    price: Int!
    numOfGuests: Int!
    numOfBeds: Int!
    numOfBaths: Int!
    rating: Int!
  }

  input LogInInput {
    code: String!
  }

  type Query {
    authUrl: String!
    listings: [Listing!]!
  }

  type Mutation {
    logIn(input: LogInInput): Viewer!
    logOut: Viewer!
    deleteListing(id: ID!): Listing!
  }
`;
