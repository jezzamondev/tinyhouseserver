import type { Request, Response } from 'express';
import type { IResolvers } from '@graphql-tools/utils';
import { Google } from '../../../lib/api/index.ts';
import type { Viewer, Database, User } from '../../../lib/types';
import type { LogInArgs } from './types';
import crypto from 'crypto';

const cookieOptions = {
  httpOnly: true, // not accessible to client side js
  sameSite: true, // to ensure cookie is not sent with cross site requests (CSRF attacks)
  signed: true, // create HMAC of the value and base64 encoding
  secure: process.env.NODE_ENV === 'development' ? false : true, // to ensure only sent by HTTPS
};

const HOURS = 24;
const MINS = 60;
const SECONDS = 60;
const MILLISECONDS = 1000;

const logInViaGoogle = async (
  code: string,
  token: string,
  db: Database,
  res: Response
): Promise<User | undefined> => {
  const { user } = await Google.logIn(code);

  if (!user) {
    throw new Error('Google login error');
  }

  // Names/Photos/Email Lists
  const userNamesList = user?.names;
  const userPhotosList = user?.photos;
  const userEmailsList = user?.emailAddresses;

  // User Display Name
  const userName = userNamesList?.[0]?.displayName;

  // User Id
  const userId = userNamesList?.[0]?.metadata?.source?.id;

  // User Avatar
  const userAvatar = userPhotosList?.[0]?.url;

  // User Email
  const userEmail = userEmailsList?.[0]?.value; // test this

  if (!userId || !userName || !userAvatar || !userEmail) {
    throw new Error('Google login error');
  }

  const updateRes = await db.users.findOneAndUpdate(
    { _id: userId },
    {
      $set: {
        name: userName,
        avatar: userAvatar,
        contact: userEmail,
        token,
      },
    },
    // { returnOriginal: false } // deprecated
    { returnDocument: 'after' }
  );

  // let viewer = updateRes.value; // deprecated
  let viewer = updateRes;

  if (!viewer) {
    const insertResult = await db.users.insertOne({
      _id: userId,
      token,
      name: userName,
      avatar: userAvatar,
      contact: userEmail,
      income: 0,
      bookings: [],
      listings: [],
    });

    // viewer = insertResult.ops[0]; Ops has been deprecated
    if (insertResult.acknowledged) {
      viewer = await db.users.findOne({
        _id: insertResult.insertedId,
      });
    }
  }

  res.cookie('viewer', userId, {
    ...cookieOptions,
    maxAge: 365 * HOURS * MINS * SECONDS * MILLISECONDS,
  });

  return viewer;
};

const logInViaCookie = async (
  token: string,
  db: Database,
  req: Request,
  res: Response
): Promise<User | undefined> => {
  const updateRes = await db.users.findOneAndUpdate(
    { _id: req.signedCookies.viewer },
    { $set: { token } },
    { returnDocument: 'after' }
  );

  let viewer = updateRes;

  if (!viewer) {
    res.clearCookie('viewer', cookieOptions);
  }

  return viewer;
};

export const viewerResolvers: IResolvers = {
  Query: {
    authUrl: (): string => {
      return Google.authUrl;
    },
  },
  Mutation: {
    logIn: async (
      //  We know Query and Mutation are our root object types so for this object to have a value, it needs to be declared and passed from the server configuration. We haven't configured anything to be defined here so root will be undefined and as a result, we can use the undefined type.
      _root: undefined,
      { input }: LogInArgs,
      { db, req, res }: { db: Database; req: Request; res: Response }
    ): Promise<Viewer> => {
      try {
        const code = input ? input.code : null;
        const token = crypto.randomBytes(16).toString('hex');

        const viewer: User | undefined = code
          ? await logInViaGoogle(code, token, db, res)
          : await logInViaCookie(token, db, req, res);

        if (!viewer) {
          return { didRequest: true };
        }
        return {
          _id: viewer?._id,
          token: viewer?.token,
          avatar: viewer?.avatar,
          walletId: viewer?.walletId,
          didRequest: true,
        };
      } catch (error) {
        throw new Error(`Failed to log in: ${error}`);
      }
    },
    logOut: (
      _root: undefined,
      _args: {},
      { res }: { res: Response }
    ): Viewer => {
      try {
        // most web browsers will only clear the cookie if the given options is identical
        // to those given to res.cookie() (excluding an expires or maxAge property).
        res.clearCookie('viewer', cookieOptions);
        return { didRequest: true };
      } catch (error) {
        throw new Error(`Failed to log out: ${error}`);
      }
    },
  },
  Viewer: {
    id: (viewer: Viewer): string | undefined => {
      return viewer._id;
    },
    hasWallet: (viewer: Viewer): boolean | undefined => {
      return viewer.walletId ? true : undefined;
    },
  },
};
