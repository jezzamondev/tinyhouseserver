import { ObjectId } from 'mongodb';
import type { Database } from './../../../lib/types.ts';
// import type { Listing } from '../../../__generated__/resolvers-types.ts';
import type { IResolvers } from '@graphql-tools/utils';
import type { Listing } from '../../../lib/types.ts';

export const listingResolvers: IResolvers = {
  Query: {
    listings: async (
      _root: any,
      _args: {},
      { db }: { db: Database }
    ): Promise<Listing[]> => {
      return await db.listings.find({}).toArray();
    },
  },
  Mutation: {
    deleteListing: async (
      _root: undefined,
      { id }: { id: string },
      { db }: { db: Database }
    ): Promise<Listing> => {
      const deleteRes = await db.listings.findOneAndDelete({
        _id: new ObjectId(id),
      });

      console.log(deleteRes);

      if (!deleteRes) {
        throw new Error('failed to delete listing');
      }

      return deleteRes;
    },
  },
  Listing: {
    id: (listing: Listing): string => listing._id.toString(),
  },
};
