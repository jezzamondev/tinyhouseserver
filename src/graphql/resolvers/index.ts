import merge from 'lodash.merge';
import { viewerResolvers } from './Viewer/index.ts';
import { listingResolvers } from './Listing/listing.ts';

export const resolvers = merge(viewerResolvers, listingResolvers);
