import { Collection, ObjectId } from 'mongodb';

export enum ListingType {
  Apartment = 'APARTMENT',
  House = 'HOUSE',
}

interface BookingsIndexMonth {
  [key: string]: boolean;
}

interface BookingsIndexYear {
  [key: string]: BookingsIndexMonth;
}

export interface BookingsIndex {
  [key: string]: BookingsIndexYear;
}

export interface Booking {
  _id: ObjectId;
  listing: ObjectId;
  // userid saved as string for GoogleAUth
  tenant: string;
  checkIn: string;
  checkOut: string;
}

export interface Listing {
  _id: ObjectId;
  title: string;
  description: string;
  image: string;
  // User's _id
  host: string;
  type: ListingType;
  address: string;
  country: string;
  // for geolocation, admin will be analogus to states or provinces
  admin: string;
  city: string;
  bookings: ObjectId[];
  bookingsIndex: BookingsIndex; // see docs bookingsIndex.mdx
  price: number;
  numOfGuests: number;
}

export interface User {
  // keeping as string for GoogleAuth
  _id: string;
  token: string;
  name: string;
  avatar: string;
  contact: string;
  walletId?: string;
  income: number;
  bookings: ObjectId[];
  listings: ObjectId[];
}

export interface Database {
  listings: Collection<Listing>;
  users: Collection<User>;
  bookings: Collection<Booking>;
}

export interface Viewer {
  _id?: string;
  token?: string;
  avatar?: string;
  walletId?: string;
  didRequest: boolean;
}
