## Bookings Index
In software programming, handling dates is hard. Questions like how do handle different geographic areas with different timezones, daylight savings time, leap seconds, the ability to compare times, etc. all have to be answered. Many different libraries (e.g. Moment.js) exist to help with a lot of these use cases.

With that being said though, we're not going to go through a difficult approach to look into how we can best handle how dates are captured when a booking is made to a listing. We'll introduce an index that will essentially be nested key-value pairs that captures all the dates that a listing is not available because of a previous booking.

Here's an example. Assume the following dates are booked with the dates listed here in the YYYY-MM-DD format.

```js
// 2019-01-01   year: 2019 | month: 01 | day: 01
// 2019-01-02   year: 2019 | month: 01 | day: 02
// 2019-05-31   year: 2019 | month: 05 | day: 31
```

We'll call the index we'll create to represent these dates that have already been booked `bookingsIndex`, and will look something like this:

```js
// 2019-01-01   year: 2019 | month: 01 | day: 01
// 2019-01-02   year: 2019 | month: 01 | day: 02
// 2019-05-31   year: 2019 | month: 05 | day: 31

const bookingsIndex = {
  "2019": {
    "00": {
      "01": true,
      "02": true
    },
    "04": {
      "31": true
    }
  }
};

// NOTE: the JavaScript function for getting the month returns 0 for Jan ... and 11 for Dec
```

The bookings index is to be nested key-value pairs where the first key is a reference to the year a booking is made.
```js
const bookingsIndex = {
  "2019": {
    // Bookings made in 2019
  }
};
```
The value provided to the first key is the `months` in which the booking is made.
```js
const bookingsIndex = {
  "2019": {
    "00": {
      // Bookings made in January 2019
    },
    "04": {
      // Bookings made in May 2019
    }
  }
};
```

In the example above, bookings have been made in `2019-01-01`, `2019-01-02`, and `2019-05-31` so we have the values of: 

`bookingsIndex[2019][00][01]`,

`bookingsIndex[2019][00][02]`, and

`bookingsIndex[2019][04][31]` all return true to represent this.

>The default JavaScript function for getting the month of a date returns `0` for the first month of the year and `11` for the last month of the year. This is reflected in how we prepare the keys for the "month" nested objects in bookingsIndex.

Why are we using objects here as the data structure within our index? This is because values in objects (i.e. hash values) can, on average, be accessed in constant time which is much computationally cheaper than having arrays where we have to iterate through a series of values to search for a particular element.