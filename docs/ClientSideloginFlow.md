
When Client app loads
Has cookie? (from previous session)
Login with cookie

No cookie? 
 Return viewer object as null (no login)

When logging in
  Follow the AuthODiagram Google login flow AND
  create a session token (not the same as google's API token), save to 
    - DB
    - save to sessionStorage as token
    - set headers `X-XSRF-TOKEN`` as `token` so its passed to each request



