```js
return await db.listings.find({}).toArray();
```

- when using the generated types from **generated** of `Resolver`

```js
import type { Resolver } from '../../../__generated__/resolvers-types.ts';
```

- will return this error to the toArray() function adding `WithId` type

```js
Type 'Promise<WithId<Listing>[]>' is not assignable to type 'ResolverTypeWrapper<Listing>[] | Promise<ResolverTypeWrapper<Listing>[]>
```

```js
return (await db.listings.find({}).toArray()) as unknown as Listing[];
```

- To resolve issues with graphql gen types and mongodb types, adding type casting `as unknown as Listing[]` will resolve typescript errors

- `unknown` erases the previous type checking so can cast `Listing[]`

### An easier workaround

- installed `@graphql-tools/utils` and use `Iresolver` from this package installed

```js
export const resolvers: IResolver
```
