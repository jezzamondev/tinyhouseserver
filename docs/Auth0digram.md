```mermaid
sequenceDiagram
    YourApp->>+GoogleServers: Request token
    GoogleServers-->>YourApp: Authorization code
    YourApp->>GoogleServers: Engage code for token
    GoogleServers-->>YourApp: Token Response 
    YourApp-->>GoogleServers: Use token to call Google API
```


When a user attempts to sign-in, they're taken to Google's login and consent screen.

When signed in successfully, an authorization code is returned from Google's servers to our application.

A request is made from our app to Google Servers with the authorization code to retrieve the access token of the user.

With the access token available in our app, we're able to make requests to Google APIs on behalf of the user.